package com.tridion.trivident.deployer.modules;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.tridion.configuration.Configuration;
import com.tridion.configuration.ConfigurationException;
import com.tridion.deployer.Module;
import com.tridion.deployer.ProcessingException;
import com.tridion.deployer.Processor;
import com.tridion.transport.transportpackage.TransportPackage;
import com.tridion.trivident.deployer.modules.util.XmlUtils;
import com.tridion.trivident.deployer.modules.xmladapters.XmlAdapter;
import com.tridion.trivident.deployer.modules.xmladapters.XmlAdapterLegacy;
import com.tridion.trivident.deployer.modules.xmladapters.XmlAdapterSDLWeb85;
import com.tridion.util.TCMURI;


public class TranslatePageUrlModule extends Module {

	protected static Logger logger = LoggerFactory.getLogger(TranslatePageUrlModule.class);
	public static boolean CHANGE_PHYSICAL_PATH = false;
	public static String PATH_TO_COMPONENT_IN_PAGES_XML_LEGACY = "./RenderingMetadata/Page/Components/Component";
	public static String PATH_TO_COMPONENT_IN_PAGES_XML = "./ComponentPresentations/ComponentPresentation";
	public static String COMPONENT_ID_ATTRIBUTE_LEGACY = "id";
	public static String COMPONENT_ID_ATTRIBUTE = "ComponentId";
	
	private String translatedUrlFieldName;
	private String translatedUrlCategoryName;
	private boolean enabled = true;
	private boolean limitToSharedPages = true;
	private List<TCMURI> limitToPublications = new ArrayList<TCMURI>();
	private boolean lookInCategory = false;
	
	// configuration attributes
	private static String TRANSLATED_URL_FIELD_NAME_ATTR = "TranslatedUrlFieldName";
	private static String TRANSLATED_URL_CATEGORY_NAME_ATTR = "TranslatedUrlCategoryName";
	private static String LIMIT_TO_SHARED_PAGES_ATTR = "LimitToSharedPages";
	private static String LIMIT_TO_PUBLICATIONS_ATTR = "LimitToPublications";
	private static String VERSION_ATTR = "Version";
	public static String SDLWEB85_VERSION = "SDLWeb85";
	private static String ENABLED_ATTR = "Enabled";
	private static String CONFIGURATION_ELMT = "Configuration";
	
	private XmlAdapter xmlAdapter = null;
	
	public TranslatePageUrlModule(Configuration config, Processor processor) throws ConfigurationException {
		super(config, processor);
		
		enabled = false;
		Configuration c2 = config.getChild(CONFIGURATION_ELMT);
		if (c2 == null) {
			logger.warn("TranslatePageUrlModule is incorrectly configured (" + CONFIGURATION_ELMT + " element missing inside Module - translation of URLs is disabled)");
			return;
		}
		if (!(c2.hasAttribute(TRANSLATED_URL_FIELD_NAME_ATTR) || c2.hasAttribute(TRANSLATED_URL_CATEGORY_NAME_ATTR))) {
			logger.warn("TranslatePageUrlModule is incorrectly configured (one of " + TRANSLATED_URL_FIELD_NAME_ATTR + " and " + TRANSLATED_URL_CATEGORY_NAME_ATTR + " attributes missing inside Configuration - translation of URLs is disabled)");
			return;			
		}
		
		if (c2.hasAttribute(TRANSLATED_URL_CATEGORY_NAME_ATTR)) {
			lookInCategory = true;
			translatedUrlCategoryName = c2.getAttribute(TRANSLATED_URL_CATEGORY_NAME_ATTR);
		}

		if (c2.hasAttribute(TRANSLATED_URL_FIELD_NAME_ATTR)) {
			translatedUrlFieldName = c2.getAttribute(TRANSLATED_URL_FIELD_NAME_ATTR);		
		}
		
		if (c2.hasAttribute(LIMIT_TO_SHARED_PAGES_ATTR)) {
			limitToSharedPages = c2.getAttribute(LIMIT_TO_SHARED_PAGES_ATTR).toLowerCase().equals("true") || c2.getAttribute(LIMIT_TO_SHARED_PAGES_ATTR).toLowerCase().equals("yes");
		}
		if (c2.hasAttribute(LIMIT_TO_PUBLICATIONS_ATTR)) {
			List<String> ltp = Arrays.asList(c2.getAttribute(LIMIT_TO_PUBLICATIONS_ATTR).split(","));			
			// make sure all publications are formatted as TCM URIs
			for (String p : ltp) {
				if (p.trim().startsWith("tcm:")) {
					try {
						limitToPublications.add(new TCMURI(p.trim()));
					} catch (ParseException e) {
						logger.warn("found incorrectly formatted TCM URI in configuration: " + p);
						return;
					}
				} else if (p.trim().matches("^\\d+$")) {
					TCMURI tcmUri = new TCMURI(0,Integer.parseInt(p.trim()),1,0);
					limitToPublications.add(tcmUri);
				} else {
					logger.warn("found incorrectly formatted TCM URI in configuration: " + p);
					return;					
				}
			}
			
			String version = c2.getAttribute(VERSION_ATTR);
			if (SDLWEB85_VERSION.equals(version))
			{
				xmlAdapter = new XmlAdapterSDLWeb85();
				logger.debug("using SDL Web 8.5 xml adapter");
			}
			else
			{
				xmlAdapter= new XmlAdapterLegacy();
				logger.debug("using legacy xml adapter");
			}
		}
		
		if (c2.hasAttribute(ENABLED_ATTR)) {
			enabled = Boolean.parseBoolean(c2.getAttribute(ENABLED_ATTR));
			if (!enabled) {
				logger.debug("TranslatePageUrlModule is disabled in the configuration");				
			}
		}
	}

	@Override
	public void process(TransportPackage transportPackage) throws ProcessingException {
		if (!enabled) {
			return;
		}
		
		logger.debug("called process for transport package " + transportPackage.getTransactionId().toString());
		logger.debug("physical path mode is " + (CHANGE_PHYSICAL_PATH ? "on" : "off"));

		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder;
		try {
			String pagesXmlPath = transportPackage.getLocationPath() + File.separator + "pages.xml";
			String componentsXmlPath = transportPackage.getLocationPath() + File.separator + "components.xml";
			String instructionsXmlPath = transportPackage.getLocationPath() + File.separator + "instructions.xml";
			builder = factory.newDocumentBuilder();
			Document instructionsDoc = builder.parse(instructionsXmlPath);

			Document doc = builder.parse(pagesXmlPath);
			Document componentDoc = builder.parse(componentsXmlPath);
			if (logger.isDebugEnabled()) {
				logger.debug("pages.xml:\r\n" + XmlUtils.getOuterXml(doc));
			}
			if (logger.isDebugEnabled()) {
				logger.debug("components.xml:\r\n" + XmlUtils.getOuterXml(componentDoc));
			}
			
			NodeList pages = XmlUtils.getNodeList(doc, "Pages/Page");
			
			//Map<String,String> renamedFiles = new HashMap<String,String>();
			boolean madeChanges = false;
			for (int i = 0; i < pages.getLength(); i++) {
				Element page = (Element) pages.item(i);
				
				// check if we need to process this page
				String pageId = page.getAttribute("Id");
				TCMURI pageUri = new TCMURI(pageId);
				Element owningPublication = XmlUtils.getElement(page, "OwningPublication");
				TCMURI owningPublicationUri = new TCMURI(owningPublication.getAttribute("Id"));
				if (limitToSharedPages) {
					if (pageUri.getPublicationId() == owningPublicationUri.getItemId()) {
						logger.debug("skipping page with id " + pageId + " and owning publication " + owningPublication.getAttribute("Id") + " because it is not a shared page");
						continue;
					}
				}	
				if (limitToPublications.size()>0) {
					boolean isValidPublication = false;
					for (TCMURI pub : limitToPublications) {
						if (owningPublicationUri.getItemId() == pub.getItemId()) {
							isValidPublication = true;
							break;
						}
					}
					if (! isValidPublication) {
						logger.debug("skipping page with id " + pageId + " and owning publication " + owningPublication.getAttribute("Id") + " because the owning publication does not occur in the list of valid publications");
						continue;						
					}					
				}
				
				String pagePath = XmlUtils.getInnerText(page, "Path");
				if (pagePath == null) {
					logger.error("unexpected error: no Path element found");
					return;
				}
				String pageURL = XmlUtils.getInnerText(page, "URL");

				//pagePath = pagePath.replaceAll("^/","");
				
				String translatedPagePath = getTranslatedPagePath(page, componentDoc);
				if (translatedPagePath == null) {
					logger.debug("no translated path, skipping page");
					continue;
				}
				
				Pattern pagePathPattern = Pattern.compile("^(.*)\\\\[^\\\\]+\\.([a-zA-Z0-9]+)$");
				Matcher m = pagePathPattern.matcher(pagePath);
				if (m.matches()) {
					pagePath = m.group(1) + "\\" + translatedPagePath + "." + m.group(2);
				}
				logger.debug("TRANSLATED PATH: " + pagePath);
				Pattern pageURLPattern = Pattern.compile("^(.*)/[^/]+\\.([a-zA-Z0-9]+)$");
				m = pageURLPattern.matcher(pageURL);
				if (m.matches()) {
					pageURL = m.group(1) + "/" + translatedPagePath + "." + m.group(2);
				}
				logger.debug("TRANSLATED URL: " + pageURL);
				
				Element pathElmt = XmlUtils.getElement(page, "Path");
				pathElmt.setTextContent(pagePath);
				Element urlElmt = XmlUtils.getElement(page, "URL");
				urlElmt.setTextContent(pageURL);
				madeChanges = true;
			}
			
			if (madeChanges) {
				// save pages.xml
				XmlUtils.saveXmlDocumentToFile(doc, pagesXmlPath);
				logger.debug("updated " + pagesXmlPath);
				// save the instructions.xml as well (only if we're in 'physical path' mode)
				if (CHANGE_PHYSICAL_PATH) {
					XmlUtils.saveXmlDocumentToFile(instructionsDoc, instructionsXmlPath);
					logger.debug("updated " + instructionsXmlPath);
				}
			} else {
				logger.debug("no changes made");
			}			
		} catch (ParserConfigurationException e) {
			 logger.warn("error while translating the URLs", e);
		} catch (SAXException e) {
			 logger.warn("error while translating the URLs", e);
		} catch (IOException e) {
			 logger.warn("error while translating the URLs", e);
		} catch (XPathExpressionException e) {
			logger.error("error running XPath on pages.xml", e);
		}		
		catch (javax.naming.ConfigurationException e) {
			logger.error("Configuration error in XML", e);
		} catch (ParseException e) {
			logger.error("Error parsing TCM URI", e);
		}	
	}

	private String getTranslatedPagePath(Element page, Document components) throws XPathExpressionException, javax.naming.ConfigurationException {
		if (lookInCategory) {
			return xmlAdapter.getTranslatedPagePathFromKeyword(page, components, translatedUrlCategoryName);		
		}
		return xmlAdapter.getTranslatedPagePathFromMetadata(page, components, translatedUrlFieldName);
	}	
}
