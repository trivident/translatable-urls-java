package com.tridion.trivident.deployer.modules.xmladapters;

import javax.naming.ConfigurationException;
import javax.xml.xpath.XPathExpressionException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.tridion.trivident.deployer.modules.util.XmlUtils;

public class XmlAdapterLegacy implements XmlAdapter {

	@Override
	public String getTranslatedPagePathFromMetadata(Element page, Document components, String translatedUrlFieldName) throws XPathExpressionException, javax.naming.ConfigurationException {
		NodeList complist = XmlUtils.getNodeList(page, "./RenderingMetadata/Page/Components/Component");
		for (int i = 0; i < complist.getLength(); i++) {
			Element c = (Element) complist.item(i);
			Element urlElmt = XmlUtils.getElement(components, "/Components/Component[@Id='" + c.getAttribute("id") + "']/Custom//*[local-name()='" + translatedUrlFieldName + "']");
			if (urlElmt != null) {
				return urlElmt.getTextContent();
			}
		}
		return null;
	}
	
	@Override
	public String appliesToTridionVersion() {
		return "Legacy";
	}

	@Override
	public String getTranslatedPagePathFromKeyword(Element page, Document components, String categoryName)
			throws XPathExpressionException, ConfigurationException {
		return XmlUtils.getInnerText(page, ".//Category[@Name='" + categoryName + "']/Keyword/Paths/Path");
	}
}
