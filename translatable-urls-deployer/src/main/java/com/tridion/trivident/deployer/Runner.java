package com.tridion.trivident.deployer;

import java.io.File;
import java.io.IOException;
import java.util.List;

import com.tridion.cd.core.configuration.ConfigurationLoader;
import com.tridion.cd.core.configuration.ConfigurationLoaderImpl;
import com.tridion.configuration.Configuration;
import com.tridion.configuration.ConfigurationException;
import com.tridion.deployer.ProcessingException;
import com.tridion.deployer.Processor;
import com.tridion.deployer.ProcessorFactory;
import com.tridion.deployer.ProcessorPhase;
import com.tridion.transport.transportpackage.TransportPackage;
import com.tridion.util.ZipUtils;

/**
 * Helper class to run the SDL Web 8+ deployer from the command line. Can be used for debugging your custom deployer extensions, storage DAOs, etc.
 * @author Quirijn Slings
 *
 */
public class Runner {
	public static void main(String[] args) {
		System.out.println("Started the deployer");
		ConfigurationLoader configLoader = new ConfigurationLoaderImpl();
		ProcessorFactory pf = ProcessorFactory.getInstance();
		try {
			Configuration config = configLoader.getConfiguration("cd_deployer_conf.xml", "cd_deployer_conf.xsd");
			pf.configure(config.getChild("Processors"));

			Configuration locationConfig = config.getChild("Queue").getChild("Location");
			TransportPackage tp = getNextTransportPackage(locationConfig.getAttribute("Path"));
			if (tp == null) {
				System.out.println("no transport packages found in " + locationConfig.getAttribute("Path"));
				return;
			}
			List<Processor> processors = pf.createProcessors("Deploy", ProcessorPhase.PRE_PROCESSING);
			for (Processor processor : processors) {
				processor.process(tp);
			}
		} catch (ConfigurationException e) {
			e.printStackTrace();
		} catch (ProcessingException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private static TransportPackage getNextTransportPackage(String incomingPath) throws IOException {
		File incoming = new File(incomingPath);
		// make sure there is a work folder
		File work = new File(incoming.getParentFile().getAbsolutePath() + File.separatorChar + "work");
		if (!work.exists()) {
			work.mkdirs();
		}

		for (final File zipLocation : incoming.listFiles()) {
			System.out.println(zipLocation.getName());
			String zipFilename = zipLocation.getName();
			String destinationFileName = zipFilename.substring(0, zipFilename.length() - 4);
			ZipUtils.unzip(zipLocation, work);
			File unzipLocation = new File(work.getAbsolutePath() + File.separatorChar + destinationFileName);
			TransportPackage tp = new TransportPackage(unzipLocation);
			return tp;
		}

		return null;
	}
}
