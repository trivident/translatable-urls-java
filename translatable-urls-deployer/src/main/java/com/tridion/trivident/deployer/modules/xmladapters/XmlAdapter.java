package com.tridion.trivident.deployer.modules.xmladapters;

import javax.naming.ConfigurationException;
import javax.xml.xpath.XPathExpressionException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

public interface XmlAdapter {
	String getTranslatedPagePathFromMetadata(Element page, Document components, String translatedUrlFieldName) throws XPathExpressionException, javax.naming.ConfigurationException;
	String getTranslatedPagePathFromKeyword(Element page, Document components, String categoryName)
			throws XPathExpressionException, ConfigurationException;
	String appliesToTridionVersion();
}
