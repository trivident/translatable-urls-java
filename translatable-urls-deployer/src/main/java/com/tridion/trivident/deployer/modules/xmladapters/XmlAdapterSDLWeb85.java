package com.tridion.trivident.deployer.modules.xmladapters;

import javax.naming.ConfigurationException;
import javax.xml.xpath.XPathExpressionException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.tridion.trivident.deployer.modules.TranslatePageUrlModule;
import com.tridion.trivident.deployer.modules.util.XmlUtils;

public class XmlAdapterSDLWeb85 implements XmlAdapter {

	@Override
	public String getTranslatedPagePathFromMetadata(Element page, Document components, String translatedUrlFieldName) throws XPathExpressionException, javax.naming.ConfigurationException {
			NodeList cpList = XmlUtils.getNodeList(page, "./ComponentPresentations/ComponentPresentation");
			for (int i = 0; i < cpList.getLength(); i++) {
				Element cp = (Element) cpList.item(i);
				Element urlElmt = XmlUtils.getElement(components, "/Components/Component[@Id='" + cp.getAttribute("ComponentId") + "']/Custom//*[local-name()='" + translatedUrlFieldName + "']");
				if (urlElmt != null) {
					return urlElmt.getTextContent();
				}
			}
			return null;
	}
	
	@Override
	public String appliesToTridionVersion() {
		return TranslatePageUrlModule.SDLWEB85_VERSION;
	}

	@Override
	public String getTranslatedPagePathFromKeyword(Element page, Document components, String categoryName)
			throws XPathExpressionException, ConfigurationException {
		return XmlUtils.getInnerText(page, ".//Category[@Name='" + categoryName + "']/Keyword/Paths/Path");
	}
}
